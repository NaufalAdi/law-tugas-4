from fastapi import Body, FastAPI
import motor.motor_asyncio
import certifi
import uvicorn
from typing import Optional
from jose import JWTError, jwt
from pydantic import ValidationError
from datetime import datetime, timedelta
from passlib.context import CryptContext
from fastapi.encoders import jsonable_encoder
from fastapi import Depends, HTTPException, status

from model import Data

app = FastAPI()

# Mongo Config
MONGODB_URL = 'mongodb+srv://naufaladi:It88F4yEXmWhN0fh@cluster0.uf3yq.mongodb.net/?retryWrites=true&w=majority'
DB_NAME = 'tugas4'

ca = certifi.where()
client = motor.motor_asyncio.AsyncIOMotorClient(
    MONGODB_URL, serverSelectionTimeoutMS=5000, tlsCAFile=ca)

try:
    db = client[DB_NAME]
except Exception:
    print("Unable to connect to MongoDB")


@app.put("/")
async def update(data: Data = Body(...),):
    data = {"npm": data.npm, "nama": data.nama}
    print(data)
    result = await db['data'].update_one({'npm': data['npm']}, {"$set": data})
    return {'status': 'OK'}


@app.get("/")
async def root():
    return {"message": "Welcome"}


if __name__ == '__main__':
    print("starting update")
    uvicorn.run("main:app", port=35871, reload=True)
