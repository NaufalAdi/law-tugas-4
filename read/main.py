from fastapi import FastAPI
import motor.motor_asyncio
import certifi
import uvicorn
from typing import Optional
from jose import JWTError, jwt
from pydantic import ValidationError
from datetime import datetime, timedelta
from passlib.context import CryptContext
from fastapi.encoders import jsonable_encoder
from fastapi import Depends, HTTPException, status

app = FastAPI()

# Mongo Config
MONGODB_URL = 'mongodb+srv://naufaladi:It88F4yEXmWhN0fh@cluster0.uf3yq.mongodb.net/?retryWrites=true&w=majority'
DB_NAME = 'tugas4'

ca = certifi.where()
client = motor.motor_asyncio.AsyncIOMotorClient(
    MONGODB_URL, serverSelectionTimeoutMS=5000, tlsCAFile=ca)

try:
    db = client[DB_NAME]
except Exception:
    print("Unable to connect to MongoDB")


@app.get("/{npm}")
async def get_data(npm: str):
    if npm:
        result = await db['data'].find_one({'npm': npm})
        return {"npm": result["npm"], "nama": result["nama"]}
    else:
        return {"message": "welcome"}


@app.get("/")
async def root():
    return {"message": "Welcome"}


@app.get("/echo")
async def root(param: str = ''):
    return {"message": param}


if __name__ == '__main__':
    print("starting read")
    uvicorn.run("main:app", port=25871, reload=True)
