from datetime import datetime
from enum import Enum
from bson.objectid import ObjectId

from fastapi import File, UploadFile
from pydantic import BaseModel, EmailStr, Field


class Data(BaseModel):
    npm: str = Field(..., max_length=50)
    nama: str = Field(...)
